import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Parking } from "../register-parking/model/register-parking.model";

@Component({
  selector: 'app-list-parking',
  templateUrl: './list-parking.component.html',
  styleUrls: ['./list-parking.component.css']
})
export class ListParkingComponent implements OnInit {

  allParking: Parking[] = []

  constructor(private router: Router) { }

  ngOnInit(): void {
    var formString = localStorage.getItem('list-parking') || " "
    this.allParking = JSON.parse(formString)
  }

  finishParking(parking : Parking){
    parking.finish = new Date().toISOString()
    var entrada = new Date(parking.entrada)
    var finish = new Date()
    const parkingDiff = Math.abs(entrada.getTime() - finish.getTime())
    const parkingTime = Math.ceil(parkingDiff / (1000 * 60 * 60))
    parking.total = ( parkingTime * 4.5 )
    
    localStorage.setItem('list-parking', JSON.stringify(this.allParking))
  }
  
  fomartDate(date ?: string){
    if(!date){
      return ''
    }
    return new Date(date).toLocaleString()
  }

}
