import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Parking } from './model/register-parking.model';

@Component({
  selector: 'app-register-parking',
  templateUrl: './register-parking.component.html',
  styleUrls: ['./register-parking.component.css']
})
export class RegisterParkingComponent implements OnInit {


  parkingForm = this.formBuilder.group({
    veiculo: ['',  [ Validators.required, Validators.minLength(3) ]  ],
    placa: ['', [ Validators.required, Validators.minLength(7), Validators.pattern("[a-zA-Z]{3}[0-9]{4}") ] ],
    motorista: ['', [ Validators.required, Validators.minLength(3) ] ],
    telefone: ['', [ Validators.required, Validators.minLength(10) ] ]
  })
  
  allParking: Parking[] = []

  constructor(private router: Router, private formBuilder: FormBuilder) {   }

  ngOnInit(): void {
    let list = localStorage.getItem('list-parking')
    if  (list != null){
      this.allParking = JSON.parse(list)
    }
  }

  goToList(){
    this.router.navigateByUrl('list-parking')
  }

  save(){
    if (this.parkingForm.valid) {
      let veiculo = this.parkingForm.value.veiculo
      let placa = this.parkingForm.value.placa
      let motorista = this.parkingForm.value.motorista
      let telefone = this.parkingForm.value.telefone
      let entrada = new Date().toISOString()
     
      
      let parking = new Parking(veiculo, placa, motorista, telefone, entrada)
      this.allParking.push(parking)

      localStorage.setItem('list-parking', JSON.stringify(this.allParking))
      this.router.navigateByUrl('list-parking')
      
    }
  }

  

}