export class Parking{
    veiculo: string
    placa: string
    motorista: string
    telefone: string
    entrada: string
    finish ?: string
    total ?: number = 0
   
   
    constructor(veiculo: string, placa: string, motorista: string, telefone: string, entrada: string, finish ?: string, total ?: number){
        this.veiculo = veiculo
        this.placa = placa
        this.motorista = motorista
        this.telefone = telefone
        this.entrada = entrada
        this.finish = finish  
        this.total = total   
    }

}